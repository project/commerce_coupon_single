<?php

/**
 * Coupons admin settings form.
 */
function commerce_coupon_single_admin() {
  $form = array();

  $form['commerce_coupon_single_coupon'] = array(
    '#type' => 'checkbox',
    '#title' => t('One single coupon per order'),
    '#description' => t('By clicking this, the cart will only allow one coupon, the applied coupon list will dissapear too.'),

    '#default_value' => variable_get('commerce_coupon_single_coupon', TRUE),
  );
  return system_settings_form($form);
}
